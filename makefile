SHELL := /bin/bash
bkpath:=$$HOME/tmux-bk-$(shell date +"%Y%m%d-%H-%M-%S")

here  :=$(shell pwd)

all: link

clone: backup clean
	git clone https://github.com/tmux-plugins/tpm tmux/plugins/tpm

backup: 
	@echo backup your old tmux setup into ${bkpath} ...
	mkdir -p ${bkpath}
	- mv $$HOME/.tmux* ${bkpath}


link: clone
	@echo link ...
	ln -s ${here}/tmux $$HOME/.tmux
	ln -s ${here}/tmux.conf $$HOME/.tmux.conf

clean:
	rm -rf tmux
